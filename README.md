## MERN Flix

A MERN Stack Movies and Series web-app.

*mernflix repository is divided into the [front-end](https://bitbucket.org/ee12099/react-app) repository (A REACT app) and the [back-end](https://bitbucket.org/ee12099/flix-service) repository*

---

## MERN Stack
 **MongoDB, Seneca, Express, React, Node**

A [MERN Stack](https://www.mongodb.com/blog/post/the-modern-application-stack-part-1-introducing-the-mean-stack)

* [MongoDB](https://www.mongodb.com/what-is-mongodb)  - Document Database

* [Express](https://expressjs.com/)  - Web Application Framework

* [React](https://reactjs.org/)  - Front-end Framework

* [Node](https://nodejs.org/en/) - JavaScript runtime environment

** Other **

* [Seneca](http://senecajs.org/)  - Micro-services Toolkit

* [Multer](https://github.com/expressjs/multer) -  Node.js middleware for handling multipart/form-data

